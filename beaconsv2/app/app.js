import {App, Platform} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {TabsPage} from './pages/tabs/tabs';


@App({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  config: {} // http://ionicframework.com/docs/v2/api/config/Config/
})
export class MyApp {
  static get parameters() {
    return [[Platform]];
  }

  constructor(platform) {
    this.rootPage = TabsPage;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      //test if the plugin is working and bluetooth is enabled
      cordova.plugins.locationManager.isBluetoothEnabled()
      .then(function(isEnabled){
          console.log("isEnabled: " + isEnabled);
          if (isEnabled) {
              alert("Bluetooth is enabled!!")
          } else {
              alert("Bluetooth was not enabled, now it is..")
              cordova.plugins.locationManager.enableBluetooth();
          }
      })
      .fail(function(e) { console.error(e); })
      .done();

      StatusBar.styleDefault();
    });
  }
}
