import {Page} from 'ionic-angular';
import {BLE} from 'ionic-native';

@Page({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {

    constructor()
    {
        startScanning();
        return BLE.scan();
    }

    startScanning() {
      BLE.scan([], 500, function(device) {
        console.log(JSON.stringify(device));
      }, failure);
    }
}
