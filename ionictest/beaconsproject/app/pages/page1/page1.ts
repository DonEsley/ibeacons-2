import {Alert, Page, NavController} from 'ionic-angular';
import {BLE} from 'ionic-native';
import {NgIf} from 'angular2/common';

@Page({
  templateUrl: 'build/pages/page1/page1.html',
})
export class Page1 
{
    id = "Onbekend";
    rssi = "Onbekend";

    constructor(public nav: NavController) {}

  	scan() {
  		BLE.isEnabled().then(data => { 
  			BLE.scan([],10).subscribe(data => {
                    this.id = String(data.id);
                    this.rssi = String(data.rssi);
    		});
  		});
  	}
}

